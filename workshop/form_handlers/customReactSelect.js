const { handlers } = require('kakunin');

class CustomReactSelectHandler {
  constructor() {
    this.optionsSelector = by.css('.select__option');
    this.selectedOptionSelector = by.css('input[type="hidden"]');
  }

  isSatisfiedBy(element, elementName) {
    return Promise.resolve(elementName.endsWith('CustomReactSelect'));
  }

  handleFill(page, elementName, desiredValue) {
    return browser.executeScript('arguments[0].scrollIntoView(false);', page[elementName].getWebElement())
      .then(() => page[elementName].click()
        .then(() => {
          const filtered = page[elementName].all(this.optionsSelector).filter(elem => elem.getAttribute('data-e2e').then(value => {
            return value === desiredValue;
          }));

          return filtered.count().then((count) => {
            if (count === 0) {
              return Promise.reject(`Expected ${desiredValue} for select element ${elementName} not found`);
            }

            return filtered.first().click();
          });
        }));
  }

  handleCheck(page, elementName, desiredValue) {
    return page[elementName].element(this.selectedOptionSelector).getAttribute('value').then((value) => {
      if (value === desiredValue) {
        return Promise.resolve();
      }

      return Promise.reject(`Expected ${desiredValue} got ${value} for select element ${elementName}`);
    });
  }

  getPriority() {
    return 997;
  }
}

handlers.addHandler(new CustomReactSelectHandler());
