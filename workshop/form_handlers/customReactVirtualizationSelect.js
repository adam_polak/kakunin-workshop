const { handlers } = require('kakunin');

class CustomReactVirtualizedSelectHandler {
  constructor() {
    this.optionsSelector = by.css('.select__option');
    this.selectedOptionSelector = by.css('.select__option--active');
    this.selectPlaceholder = by.css('.Select-placeholder');
  }

  isSatisfiedBy(element, elementName) {
    return Promise.resolve(elementName.endsWith('CustomReactVirtualizedSelect'));
  }

  handleFill(page, elementName, desiredValue) {
    return browser.executeScript('arguments[0].scrollIntoView(false);', page[elementName].getWebElement())
      .then(() => page[elementName].click()
        .then(() => {
          const filtered = page[elementName].all(this.optionsSelector).filter(elem => elem.getAttribute('data-e2e').then(value => value === desiredValue));

          return filtered.count().then((count) => {
            if (count === 0) {
              return Promise.reject(`Expected ${desiredValue} for select element ${elementName} not found`);
            }

            return filtered.first().click();
          });
        }));
  }

  handleCheck(page, elementName, desiredValue) {
    return page[elementName].click()
      .then(() => page[elementName].element(this.selectedOptionSelector).isPresent())
      .then((isPresent) => {
        if (isPresent) {
          return page[elementName].element(this.selectedOptionSelector).getAttribute('data-e2e').then((value) => {
            if (value === desiredValue) {
              return Promise.resolve();
            }

            return Promise.reject(`Expected ${desiredValue} got ${value} for select element ${elementName}`);
          }).then(() => page[elementName].click());
        }

        return page[elementName].element(this.selectPlaceholder).isPresent()
          .then((isPresent) => {
            if (isPresent) {
              return Promise.resolve();
            }

            return Promise.reject(`Expected ${elementName} is not available.`);
          }).then(() => page[elementName].click());
      });
  }

  getPriority() {
    return 997;
  }
}

handlers.addHandler(new CustomReactVirtualizedSelectHandler());
