const { handlers } = require('kakunin');

class CustomReactCheckboxHandler {
  constructor() {
    this.optionsSelector = by.css('label.form__checkbox');
    this.selectedOptionSelector = by.css('label.form__checkbox.checked');
  }

  isSatisfiedBy(element, elementName) {
    return Promise.resolve(elementName.endsWith('ReactCheckboxSelected'));
  }

  handleFill(page, elementName, desiredValue) {
    const filtered = page[elementName].all(this.optionsSelector).filter(elem => elem.getAttribute('data-e2e').then(value => value === desiredValue));
    const filteredSelected = page[elementName].all(this.selectedOptionSelector).filter(elem => elem.getAttribute('data-e2e').then(value => value === desiredValue));

    return browser.executeScript('arguments[0].scrollIntoView(false);', page[elementName].getWebElement())
      .then(() => {
        return filtered.count().then((count) => {
          if (count === 0) {
            return Promise.reject(`Expected ${desiredValue} for select element ${elementName} has not beed found.`);
          }

          return filteredSelected.count().then((countSelected) => {
            if (countSelected === 0) {
              return filtered.first().click();
            }

            return Promise.resolve();
          });
        });
      });
  }

  handleCheck(page, elementName, desiredValue) {
    const filteredElements = page[elementName].all(this.selectedOptionSelector);

    return filteredElements.count()
      .then((count) => {
        if (desiredValue === '') {
          if (count === 0) {
            return Promise.resolve();
          }

          return Promise.reject(`Expected count to be 0 got ${count}`);
        }

        return filteredElements.filter(element => element.getAttribute('data-e2e').then(value => value === desiredValue)).count().then((count) => {
          if (count === 1) {
            return Promise.resolve();
          }

          return Promise.reject(`Expected element ${elementName} to be selected`);
        });
      });
  }

  getPriority() {
    return 997;
  }
}

handlers.addHandler(new CustomReactCheckboxHandler());
