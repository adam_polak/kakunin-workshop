const { BasePage } = require('kakunin');

class MyRequestsPage extends BasePage {
  constructor() {
    super();

    this.url = '/my-requests';

    this.latestRequestIdNumber = $$('.my-requests__request').get(0).$('p.id.request-detail');
    this.myRequests = $('.my-requests');

    this.myRequestsContent = $('.content.my-requests__request');
    this.viewFirstRequest = $$('.view').get(0);
  }
}

module.exports = new MyRequestsPage();
