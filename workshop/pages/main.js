const { FormPage } = require('kakunin');

class MainPage extends FormPage {
  constructor() {
    super();

    this.url = '/';

    this.login = $('.navigation-login');

    this.joinOurStore = $('a[href="/join-our-store"]');
    this.myRequests = $('[href="/my-requests"]');
  }
}

module.exports = new MainPage();
