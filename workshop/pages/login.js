const { BasePage } = require('kakunin');

class LoginPage extends BasePage {
  constructor() {
    super();

    this.url = '/login';

    this.inputEmail = $('[type="email"]');
    this.inputPassword = $('[type="password"]');
    this.loginSubmitButton = $('button');
    // Deactivated user
    this.changeAccountButton = $('[name="go-to-login"]');
  }


  setEmail(email) {
    return this.inputEmail.sendKeys(email);
  }

  setPassword(password) {
    return this.inputPassword.sendKeys(password);
  }

  submitLogin() {
    return this.loginSubmitButton.click();
  }

  login(email, password) {
    const self = this;

    return protractor.browser.sleep(2000).then(() => Promise.all([
        self.setEmail(email),
        self.setPassword(password),
        self.submitLogin(),
      ])
    );
  }
}

module.exports = new LoginPage();
module.exports.classDef = LoginPage;
