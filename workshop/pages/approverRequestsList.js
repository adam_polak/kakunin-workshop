const { FormPage } = require('kakunin');

class ApproverRequestsList extends FormPage {
  constructor() {
    super();

    this.url = '/requests-list';

    this.requestTable = $('table.requests-table');
    this.requestTableRow = $$('.requests-table__row');
    this.needsAttentionTab = $('[id="react-tabs-2"]');
    this.needsDecisionTab = $('[id="react-tabs-6"]');

    this.selectedCheckbox = by.css('.form__checkbox--no-label.checked');
    this.id = by.css('.id');
    this.name = by.css('.name');
    this.daysToRespond = by.css('p.days-to-respond');
    this.proposerName = by.css('.proposer-name');
    this.date = by.css('.date');
    this.statusCustomReactVirtualizedSelect = $$('.status-select').get(0);
    this.statusSecondCustomReactVirtualizedSelect = $$('.status-select').get(1);
    this.download = by.css('.download i');
    this.print = by.css('.print i');
    this.firstRequestId = $$('.id').get(0);
    this.firstRequestDownload = $$('.download').get(0);

    this.loader = $('.requests-list__loading-more');
    this.modal = $('.modal');
  }
}

module.exports = new ApproverRequestsList();
