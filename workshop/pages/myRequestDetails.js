const { BasePage } = require('kakunin');

class MyRequestsDetailsPage extends BasePage {
  constructor() {
    super();

    this.url = '/my-requests/:id';

    this.requestDetailsPage = $('.request-details__content');
    this.backButton = $('button[name="go-back"]');
    this.requestDetails = $$('.content__container');
    this.requestName = by.css('.request-details__name');
    this.userName = by.css('.request-details__user-name');
    this.userEmail = by.css('.request-details__user-email');
    this.userMobile = by.css('.request-details__user-mobile');
  }
}

module.exports = new MyRequestsDetailsPage();
