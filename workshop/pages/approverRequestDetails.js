const { BasePage } = require('kakunin');

class ApproverRequestDetails extends BasePage {
  constructor() {
    super();

    this.url = '/request/:id';

    this.detailsView = $$('.content__container');

    this.backButton = $('[name="go-back"]');
    this.title = by.css('.request-details__name--large');
    this.proposerName = by.css('.request-details__proposer-name');
    this.description = by.css('.request-details__request-description');
    this.reviewerName = by.css('.request-details__reviewer-name');
    this.viewDetailsButton = $('[name="view-details"]');
    this.comment = by.css('.request-details__request-comment');

    this.approverRateSelected = $$('.request-details__rating--approver .rating-category__element--selected');
    this.userFriendlyRateSelected = $$('.rate-box__userFriendly .rating-category__element--selected');
    this.functionalityRateSelected = $$('.rate-box__functionality .rating-category__element--selected');
    this.tractionRateSelected = $$('.rate-box__traction .rating-category__element--selected');
    this.technologyRateSelected = $$('.rate-box__technology .rating-category__element--selected');
    this.readinessRateSelected = $$('.rate-box__readiness .rating-category__element--selected');
    this.averageRateSelected = $$('.rate-box__average .rating-category__element--selected');
  }
}

module.exports = new ApproverRequestDetails();
