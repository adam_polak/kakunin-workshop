const { FormPage } = require('kakunin');

class JoinOurStorePage extends FormPage {
  constructor() {
    super();

    const createErrorSelector = selector => $(selector).element(by.xpath('..')).$('.form__error');

    this.url = '/join-our-store';

    this.joinOurStoreForm = $('form');
    // Store Details
    this.name = $('input[name="name"]');
    this.description = $('textarea.form__textarea');
    this.serviceMarketCustomReactSelect = $('.serviceMarket .Select');
    this.serviceCategoryCustomReactSelect = $('.serviceCategory .Select');
    this.targetMarketSegmentCustomReactCheckboxSelected = $('.targetMarketSegment');
    this.targetMarketSegmentCustomReactCheckboxUnselected = $('.targetMarketSegment');
    this.serviceTypeCustomReactSelect = $('.serviceType.request-form__checkbox-group--col-2');
    // Business Realization
    this.projectUrl = $('input[name="projectUrl"]');
    this.businessRealizationText = $('.businessRealization .form-section__description');
    this.commercializationCustomReactSelect = $('.commercialization .Select');
    this.readinessCustomReactSelect = $('.readiness .Select');
    this.valuePropositionCustomReactSelect = $('.valueProposition .Select');
    this.competitorsCustomReactSelect = $('.competitors .Select');
    this.addAttachment = $('.add-attachment__button input');
    this.addAttachmentButton = $('.add-attachment__button');
    this.firstAttachment = $$('.attachments-list__attachment').get(0);
    this.secondAttachment = $$('.attachments-list__attachment').get(1);
    this.thirdAttachment = $$('.attachments-list__attachment').get(2);
    // Terms and agreement
    this.termsAndConditionsLink = $('.request-form__terms-and-agreement a');

    this.termsAndConditionsCheckbox = $('.request-form__terms-and-agreement i[class="material-icons"]');
    this.acceptedTermsAndConditions = $('.request-form__terms-and-agreement .form__checkbox.checked');

    this.submit = $('button[name="submit"]');
    this.confirmButton = $('button.modal__button.modal__button--confirm');
    this.submissionConfirmation = $('.submission-confirmation');
    this.storeNumber = $('.submission-confirmation span');
    this.myRequests = $('button[name="go-to-my-requests"]');
    // Validation
    this.nameValidationMsg = $('.name .form__error');
    this.targetMarketSegmentValidationMsg = createErrorSelector('.targetMarketSegment');
  }
}

module.exports = new JoinOurStorePage();
