const { defineSupportCode } = require('kakunin');

defineSupportCode(({ Given }) => {
  Given(/^I am logged in as an "([^"]*)"$/, function (user) {
    this.currentUser = {
      account: this.userProvider.getUser(user),
      type: user,
    };

    const mainPage = browser.page.main;
    const loginPage = browser.page.login;
    const self = this;
    const footer = element(by.css('.footer'));

    return browser.wait(protractor.ExpectedConditions['visibilityOf'](loginPage['inputEmail'], 2000))
      .then(() => loginPage.login(self.currentUser.account.email, self.currentUser.account.password))
      .then(() => expect(mainPage.isPresent('login')).not.to.eventually.be.ok)
      .then(() => browser.wait(protractor.ExpectedConditions['visibilityOf'](footer, 2000)))
      .then(() => element(by.css(`[data-e2e="en"]`)).click())
      .then(() => {
        self.currentPage = mainPage;
        return Promise.resolve();
      });
  });
});

