@approversRequestsLists @approver
Feature: Approver's requests list
    I want to have all requests displayed
    As a approver
    In order to manage them

    Background:
        Given I visit the "main" page
        And the "login" page is displayed
        And I wait for "visibilityOf" of the "loginSubmitButton" element
        And I am logged in as an "approver"
        And the "approverRequestsList" page is displayed
        And I wait for "visibilityOf" of the "requestTable" element

    @acceptance @reloadFixtures @downloadClearBefore @downloadClearAfter
    Scenario: View all requests as a approver and check sorting by dates
        When I store the "firstRequestId" element text as "storedStoreNumber" variable
        And I click the "firstRequestDownload" element
        Then the file "v:storedStoreNumber.zip" should be downloaded
        And there are "equal 20" following elements for element "requestTableRow":
            | element       | value                |
            | id            | r:storeId            |
            | name          | r:notEmpty           |
            | daysToRespond | r:daysToRespond      |
            | proposerName  | r:notEmpty           |
            | date          | r:standardDateFormat |
            | download      | f:isVisible          |
            | print         | f:isVisible          |
        And the "requestTable" form is filled with:
            | statusCustomReactVirtualizedSelect | d:status-request-dictionary:rejected |
        When I infinitely scroll to the "loader" element
        Then "date" value on the "requestTableRow" list is sorted in "descending" order
        And there are "at least 21" following elements for element "requestTableRow":
            | element | value     |
            | id      | r:storeId |

    @acceptance @reloadFixtures @status @validation
    Scenario: Change statuses on all of the tabs
        When I store the "firstRequestId" element text as "storedStoreNumber" variable
        And I fill the "requestTable" form with:
            | statusCustomReactVirtualizedSelect | d:status-request-dictionary:under-review |
        And I wait for "invisibilityOf" of the "modal" element
        Then the "requestTable" form is filled with:
            | statusCustomReactVirtualizedSelect | d:status-request-dictionary:under-review |
        When I click the "needsAttentionTab" element
        And I wait for the "requestTable" element to disappear
        And I wait for "visibilityOf" of the "requestTable" element
        And I fill the "requestTable" form with:
            | statusCustomReactVirtualizedSelect | d:status-request-dictionary:new |
        And I wait for "invisibilityOf" of the "modal" element
        Then there is element "firstRequestId" with value "t:v:storedStoreNumber"

    @acceptance @ratingDetails @reloadFixtures
    Scenario: View rated request
        When I click the "needsDecisionTab" element
        And I wait for "visibilityOf" of the "requestTable" element
        And I click the "firstRequestId" element
        Then the "approverRequestDetails" page is displayed
        When I wait for "visibilityOf" of the "backButton" element
        Then there are "at least 1" following elements for element "detailsView":
            | element      | value                        |
            | title        | t:Idea 9 - STORE-170101-0009 |
            | proposerName | r:notEmpty                   |
            | description  | t:Idea 9                     |
        And the rate is set:
            | approverRateSelected      | 6 |
            | userFriendlyRateSelected  | 8 |
            | functionalityRateSelected | 2 |
            | tractionRateSelected      | 3 |
            | technologyRateSelected    | 6 |
            | readinessRateSelected     | 9 |
            | averageRateSelected       | 6 |
        When I click the "viewDetailsButton" element
        Then there are "at least 1" following elements for element "detailsView":
            | element      | value                        |
            | title        | t:Idea 9 - STORE-170101-0009 |
            | proposerName | r:notEmpty                   |
            | description  | t:Idea 9                     |
            | reviewerName | r:notEmpty                   |
            | comment      | r:notEmpty                   |
        And the rate is set:
            | userFriendlyRateSelected  | 8 |
            | functionalityRateSelected | 2 |
            | tractionRateSelected      | 3 |
            | technologyRateSelected    | 6 |
            | readinessRateSelected     | 9 |
            | averageRateSelected       | 6 |
