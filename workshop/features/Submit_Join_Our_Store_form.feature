@joinOurStore @submitter
Feature: Submit Join Our Store form
    I want to be able to fill the form
    As a submitter
    In order to submit it later

    Background:
        Given I visit the "main" page
        And the "login" page is displayed
        And I wait for "visibilityOf" of the "loginSubmitButton" element
        And I am logged in as an "submitterWithCompletedProfile"
        And I click the "joinOurStore" element
        And the "joinOurStore" page is displayed
        And I wait for "visibilityOf" of the "joinOurStoreForm" element

    @userRedirection @smoke
    Scenario: Fill and submit Join Our Store form
        Then the "addAttachmentButton" element is present
        And the "submit" element is disabled
        When I fill the "joinOurStoreForm" form with:
            | name                                           | Name                                                    |
            | description                                    | g:stringWithLength:10                                   |
            | serviceMarketCustomReactSelect                 | d:service-market-dictionary:SME-services                |
            | serviceCategoryCustomReactSelect               | d:service-category-dictionary:home-based-business       |
            | targetMarketSegmentCustomReactCheckboxSelected | d:target-market-segment-dictionary:all                  |
            | serviceTypeCustomReactSelect                   | d:service-type-dictionary:online-service                |
            | projectUrl                                     | http://random.url                                       |
            | commercializationCustomReactSelect             | d:commercialization-dictionary:patent                   |
            | readinessCustomReactSelect                     | d:readiness-dictionary:prototype                        |
            | valuePropositionCustomReactSelect              | d:value-proposition-dictionary:integrates-with-services |
            | competitorsCustomReactSelect                   | d:competitors-dictionary:low                            |
            | addAttachment                                  | logo.png                                                |
            | addAttachment                                  | logo0.png                                               |
            | addAttachment                                  | logo1.png                                               |
        And I wait for "visibilityOf" of the "firstAttachment" element
        And I wait for "visibilityOf" of the "secondAttachment" element
        And I wait for "visibilityOf" of the "thirdAttachment" element
        Then the "addAttachmentButton" element is not present
        And the "joinOurStoreForm" form is filled with:
            | name                                           | Name                                                    |
            | serviceMarketCustomReactSelect                 | d:service-market-dictionary:SME-services                |
            | serviceCategoryCustomReactSelect               | d:service-category-dictionary:home-based-business       |
            | targetMarketSegmentCustomReactCheckboxSelected | d:target-market-segment-dictionary:all                  |
            | serviceTypeCustomReactSelect                   | d:service-type-dictionary:online-service                |
            | projectUrl                                     | http://random.url                                       |
            | commercializationCustomReactSelect             | d:commercialization-dictionary:patent                   |
            | readinessCustomReactSelect                     | d:readiness-dictionary:prototype                        |
            | valuePropositionCustomReactSelect              | d:value-proposition-dictionary:integrates-with-services |
            | competitorsCustomReactSelect                   | d:competitors-dictionary:low                            |
        # because currently there is not possibility to check r:notEmpty in 'filled form' step
        And there is element "description" with value "r:notEmpty"
        And there is element "termsAndConditionsLink" with value "attribute:href:termsAndConditionsLinkRegex"
        And there is element "termsAndConditionsLink" with value "f:isClickable"
        When I click the "termsAndConditionsCheckbox" element
        And I click the "submit" element
        And I click the "confirmButton" element
        And I wait for "visibilityOf" of the "submissionConfirmation" element
        And I store the "storeNumber" element text matched by "r:matchingStoreId" as "storedStoreNumber" variable
        Then the email has been sent and contains:
            | currentUser |                       |  |  |
            | subject     | r:emailSubject        |  |  |
            | html_body   | r:matchingStoreId     |  |  |
            | html_body   | t:25 days             |  |  |
            | html_body   | t:v:storedStoreNumber |  |  |

    @smoke
    Scenario: View details of previously submitted form
        Given I visit the "myRequests" page
        Then the "myRequests" page is displayed
        When I wait for "visibilityOf" of the "myRequestsContent" element
        And I click the "viewFirstRequest" element
        Then the "myRequestDetails" page is displayed
        When I wait for "visibilityOf" of the "requestDetailsPage" element
        Then there are following elements in table "requestDetails":
            | requestName | userName   | userEmail                 | userMobile     |
            | r:notEmpty  | r:notEmpty | t:910ths.est2@yopmail.com | t:966123123123 |


    @smoke @validation
    Scenario: Validation on the Join Our Store form
        When I fill the "joinOurStoreForm" form with:
            | targetMarketSegmentCustomReactCheckboxSelected | d:target-market-segment-dictionary:all |
        When I fill the "joinOurStoreForm" form with:
            | targetMarketSegmentCustomReactCheckboxUnselected | d:target-market-segment-dictionary:all |
        And I click the "name" element
        And I click the "description" element
        Then the error messages should be displayed:
            | element                          | errorMessage                    |
            | nameValidationMsg                | This value should not be blank. |
            | targetMarketSegmentValidationMsg | This value should not be blank. |
