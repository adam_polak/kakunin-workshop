const { dictionaries } = require('kakunin');

class CompetitorsDictionary {
  constructor() {
    this.values = {
      'low': 'COMPETITORS.LOW',
    };

    this.name = 'competitors-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new CompetitorsDictionary());
