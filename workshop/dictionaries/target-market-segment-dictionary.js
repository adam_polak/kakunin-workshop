const { dictionaries } = require('kakunin');

class TargetMarketSegmentDictionary {
  constructor() {
    this.values = {
      'all': 'TARGET_MARKET_SEGMENT.ALL',
      'smes': 'TARGET_MARKET_SEGMENT.SMES',
      'eldery': 'TARGET_MARKET_SEGMENT.ELDERY',
      'females': 'TARGET_MARKET_SEGMENT.FEMALES',
      'public': 'TARGET_MARKET_SEGMENT.PUBLIC',
      'youth': 'TARGET_MARKET_SEGMENT.YOUTH',
      'males': 'TARGET_MARKET_SEGMENT.MALES',
    };

    this.name = 'target-market-segment-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new TargetMarketSegmentDictionary());
