const { dictionaries } = require('kakunin');

class ServiceCategoryDictionary {
  constructor() {
    this.values = {
      'empty': '',
      'home-based-business': '/app_e2e.php/service_categories/27a3e294-a30b-4afc-8da2-100ddfeadba0',
      'financial-solutions': '/app_e2e.php/service_categories/1dcdd125-9eaa-477b-a308-cc89fd0c96ab',
    };

    this.name = 'service-category-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new ServiceCategoryDictionary());
