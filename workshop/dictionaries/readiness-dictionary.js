const { dictionaries } = require('kakunin');

class ReadinessDictionary {
  constructor() {
    this.values = {
      'prototype': 'READINESS.PROTOTYPE',
    };

    this.name = 'readiness-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new ReadinessDictionary());
