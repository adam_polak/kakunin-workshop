const { dictionaries } = require('kakunin');

class ServiceMarketDictionary {
  constructor() {
    this.values = {
      'SME-services': '/app_e2e.php/service_markets/80a06089-2f94-4992-8f7c-1d30fc7b8a3e',
    };

    this.name = 'service-market-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new ServiceMarketDictionary());
