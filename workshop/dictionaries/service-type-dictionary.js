const { dictionaries } = require('kakunin');

class ServiceTypeDictionary {
  constructor() {
    this.values = {
      'empty': '',
      'online-service': '/app_e2e.php/service_types/2089fbe1-56f5-4e16-b2ec-04df64e3f456',
      'ios-app': '/app_e2e.php/service_types/685e3a3a-1317-45d6-bdbb-7c44655a5b7a',
      'android-app': '/app_e2e.php/service_types/4c2284da-ab8a-430d-a983-07b0041051aa',
    };

    this.name = 'service-type-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new ServiceTypeDictionary());
