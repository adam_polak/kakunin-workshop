const { dictionaries } = require('kakunin');

class CommercializationDictionary {
  constructor() {
    this.values = {
      'patent': 'COMMERCIALIZATION.PATENT',
    };

    this.name = 'commercialization-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new CommercializationDictionary());
