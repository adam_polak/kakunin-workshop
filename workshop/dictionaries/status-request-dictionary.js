const { dictionaries } = require('kakunin');

class StatusRequestDictionary {
  constructor() {
    this.values = {
      'new': '/app_e2e.php/statuses/1be2f754-ffda-4946-a30d-3b30d7e90fe7',
      'existing': '/app_e2e.php/statuses/2e5d15db-bf00-460d-9cb2-6e256b36dcd6',
      'rejected': '/app_e2e.php/statuses/1be2f754-ffda-4946-a30d-3b30d7e90fe8',
      'under-review': '/app_e2e.php/statuses/1be2f754-ffda-4946-a30d-3b30d7e90fe9',
      'assigned-for-rating': '/app_e2e.php/statuses/bf6419f3-981d-4273-82f9-e338376af343',
      'rated': '/app_e2e.php/statuses/e5e9c40e-5e2c-45dc-ba10-9f0f704def3f',
      'interview-conducted': '/app_e2e.php/statuses/60f70dce-64ed-4ffc-a2a0-67152116f108',
      'approved': '/app_e2e.php/statuses/ea650625-4bf1-460b-9c6b-de1e7d76ae77',
    };

    this.name = 'status-request-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new StatusRequestDictionary());
