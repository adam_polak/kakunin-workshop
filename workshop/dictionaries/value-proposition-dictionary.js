const { dictionaries } = require('kakunin');

class ValuePropositionDictionary {
  constructor() {
    this.values = {
      'integrates-with-services': 'VALUE_PROPOSITION.INTEGRATES_WITH_SERVICES',
    };

    this.name = 'value-proposition-dictionary';
  }

  isSatisfiedBy(name) {
    return this.name === name;
  }

  getMappedValue(key) {
    return this.values[key];
  }
}

dictionaries.addDictionary(new ValuePropositionDictionary());
