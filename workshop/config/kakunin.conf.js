module.exports = {
  type: 'otherWeb',
  baseUrl: 'http://localhost:3000',
  browserWidth: 1600,
  browserHeight: 900,
  intervalEmail: 5000,
  timeout: 100,
  elementsVisibilityTimeout: 5,
  waitForPageTimeout: 10,
  downloadTimeout: 30,
  reports: '/reports',
  downloads: '/downloads',
  data: '/data',
  features: [
    '/features',
  ],
  pages: [
    '/pages',
  ],
  matchers: [
    '/matchers',
  ],
  generators: [
    '/generators',
  ],
  form_handlers: [
    '/form_handlers',
  ],
  step_definitions: [
    '/step_definitions',
  ],
  comparators: [
    '/comparators',
  ],
  dictionaries: [
    '/dictionaries',
  ],
  regexes: [
    '/regexes',
  ],
  hooks: [
    '/hooks',
  ],
  email: {
    type: 'mailtrap',
    config: {
      apiKey: '81279e9fb9351ebdf7aefef89e6a5701',
      inboxId: '203459',
      url: 'https://mailtrap.io/api/v1'
    }
  },
  clearEmailInboxBeforeTests: true,
  clearCookiesAfterScenario: true,
  clearLocalStorageAfterScenario: false,
  accounts: {
    submitter: {
      accounts: [
        {
          email: '910ths.est1@yopmail.com',
          password: '910ths123',
          used: false,
        },
      ],
    },
    submitterEmpty: {
      accounts: [
        {
          email: '910ths.est8@yopmail.com',
          password: '910ths123',
          used: false,
        },
      ],
    },
    submitterWithCompletedProfile: {
      accounts: [
        {
          email: '910ths.est2@yopmail.com',
          password: '910ths123',
        },
      ],
    },
    submitterWithRequests: {
      accounts: [
        {
          email: '910ths.est3@yopmail.com',
          password: '910ths123',
        },
      ],
    },
    approver: {
      accounts: [
        {
          email: '910ths.est5@yopmail.com',
          password: '910ths123',
        },
      ],
    },
    admin: {
      accounts: [
        {
          email: '910ths.est6@yopmail.com',
          password: '910ths123',
        },
      ],
    },
  },
};
