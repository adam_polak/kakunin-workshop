module.exports = {
  storeId: 'STORE\-[0-9]{6}\-[0-9]{4}',
  matchingStoreId: '(STORE\-[0-9]{6}\-[0-9]{4})',
  standardDateFormat: '.*([0-9]{2}[\-\/][0-9]{2}[\-\/][0-9]{2}).*',
  daysToRespond: '([0-9])|[1-2][0-5]',
  emailSubject: '(Join Our Store - STORE-[0-9]{6}-[0-9]{4}انضم الينا -)',
  termsAndConditionsLinkRegex: '(https:\/\/592ebada91cdf84d47a77726\.preview\.siteleaf\.com\/(en|ar)\/terms-and-conditions)',
};
