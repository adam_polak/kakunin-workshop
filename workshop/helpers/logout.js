module.exports = function (clearStoredData = true) {
    return protractor.browser.executeScript('return window.localStorage.getItem("access_token");').then((token) => {
        const api = process.env.API_HOST;
        const url = `${api}/oauth/logout?token=${token}`;

        if (api === undefined) {
            throw new Error('Please specify API_HOST in `.env`');
        }

        if (!clearStoredData) {
            return protractor.browser.executeScript(`window.location.href = "${url}";`)
                .then(() => protractor.browser.executeScript('window.location.href = "http://localhost:3000";'));
        }

        return protractor.browser.manage().deleteAllCookies()
            .then(() => protractor.browser.executeScript('window.localStorage.clear();'))
            .then(() => protractor.browser.executeScript(`window.location.href = "${api}/oauth/logout?token=${token || 'noToken'}";`))
            .then(() => protractor.browser.executeScript('window.location.href = "http://localhost:3000";'));
    });
};

