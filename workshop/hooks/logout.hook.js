const logoutHelper = require('../helpers/logout');
const { defineSupportCode } = require('kakunin');

defineSupportCode(({ After }) => {
  After(() => logoutHelper());
});
