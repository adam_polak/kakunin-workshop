module.exports = {
  "extends": "airbnb",
  "rules": {
    "no-plusplus": 0,
    "no-trailing-spaces": ["error", {
      "skipBlankLines": true
    }]
  },
  "env": {
    "browser": true,
    "node": true,
    "protractor": true,
  }
};
